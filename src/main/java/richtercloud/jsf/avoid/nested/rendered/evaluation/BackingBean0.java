package richtercloud.jsf.avoid.nested.rendered.evaluation;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Random;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BackingBean0.class);
    private String property0 = null;
    private Entity0 reference = new Entity0(new Entity0(null));

    public BackingBean0() {
    }

    public String getProperty0() {
        return property0;
    }

    public void setProperty0(String property0) {
        this.property0 = property0;
    }

    public Entity0 getReference() {
        return reference;
    }

    public void setReference(Entity0 reference) {
        this.reference = reference;
    }

    public boolean checkConditionalOuter() {
        LOGGER.debug("checkConditionalOuter");
        boolean retValue = property0 != null;
        return retValue;
    }

    public boolean checkConditionalInner() {
        LOGGER.debug("checkConditionalInner");
        if(property0 == null) {
            throw new IllegalStateException("value mustn't be null");
        }
        boolean retValue = property0.equals("abc");
        return retValue;
    }

    public String checkConditionalInnerValue() {
        LOGGER.debug("checkConditionalInnerValue");
        return String.valueOf(new Random().nextInt());
    }

    public void action0(AjaxBehaviorEvent event) {
        this.property0 = "abc";
    }
}

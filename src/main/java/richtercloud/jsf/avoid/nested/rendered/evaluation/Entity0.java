package richtercloud.jsf.avoid.nested.rendered.evaluation;

/**
 *
 * @author richter
 */
public class Entity0 {
    private Entity0 reference;

    public Entity0(Entity0 reference) {
        this.reference = reference;
    }

    public Entity0 getReference() {
        return reference;
    }

    public void setReference(Entity0 reference) {
        this.reference = reference;
    }
}

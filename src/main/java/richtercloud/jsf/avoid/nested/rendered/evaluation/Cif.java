package richtercloud.jsf.avoid.nested.rendered.evaluation;

import java.lang.ref.WeakReference;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;

/**
 *
 * @author richter
 */
@FacesComponent("cif")
public class Cif extends UIComponentBase {

    public boolean checkRecurse(WeakReference reference) {
        return reference.get() != null;
    }

    @Override
    public String getFamily() {
        return "nested";
    }
}
